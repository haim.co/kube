#!/bin/bash
this_file=$(basename $0)
dir=$(cd $(dirname $0);pwd)
kube_dir=$(cd ${dir}/..;pwd)

_get_funcs()
{
    declare -F | cut -d' ' -f3- | grep -v -e ^"main"$ -e ^"_"
}

_is_func_exist()
{
    local name=$1

    for i in $(_get_funcs);do
        if [ "${name}"x = "${i}"x ];then
            return 0
        fi
    done

    return 1
}

usage()
{
    echo
    echo "usage:"
    echo "  ${this_file} <function> [arg]*"
    echo
    echo "  available function"
    echo "  ------------------"
    for i in $(_get_funcs);do
        echo "  ${i}"
    done
    return 0
}

reg_clean()
{
  cd ${kube_dir}
  vagrant ssh -c "docker exec -it registry registry garbage-collect /etc/docker/registry/config.yml --delete-untagged=true;docker system prune -f -a"
  return 0
}

reg_ls()
{
  curl -X GET http://${DOCKER_REGISTRY}/v2/_catalog 2>/dev/null | jq
  return 0
}

start()
{
  cd ${kube_dir}
  vagrant up
  return $?
}

stop()
{
  cd ${kube_dir}
  vagrant halt
  return $?
}

destroy()
{
  cd ${kube_dir}
  vagrant destroy -f
  return $?
}

status()
{
  cd ${kube_dir}
  vagrant status
  return $?
}

ssh()
{
  cd ${kube_dir}
  vagrant ssh $(vagrant status | grep '\-master' | awk '{print $1}')
  return $?
}

main()
{
  if [ "$1"x = ""x ];then
    usage
    return 0
  fi

  _is_func_exist $1
  if [ $? -ne 0 ];then
    echo "ERROR function \"${1}\" not exist"
    usage
    return 1
  fi

  $@
  return $?
}

main $@
exit $?
