## Kubernetes Cluster

1. based on ubuntu vm with latest kernel.
2. multi node support
3. private registry
4. docker host

## Requirements
please install the following:

1. VirtualBox https://www.virtualbox.org/

2. Vagrant https://www.vagrantup.com/

3. Vagrant plugins
```
vagrant plugin install vagrant-reload
vagrant plugin install vagrant-host-shell
vagrant plugin install vagrant-disksize
vagrant plugin install vagrant-vbguest
```

## one time download, configure, install
```
git clone https://gitlab.com/haim.co/kube.git
cd kube
cp config_template.yml config.yml
vagrant up
```
configuration is at config.yml

default configuration is good enough for start working.
* node_num: default to 0
    
    the amount of worker nodes in addition to the master node.
* vm_name: the prefix name for each vm created
  
    i.e: kube-master, kube-node1, kube-node2, ...
* network.forwards: port forwarding from host to master vm.
* mounts: mounting host disk to master vm.

## Let's use it
```
# source the env var for access
. <kube dir>/conf/config.sh
echo "your docker commands now running on master node: ${DOCKER_HOST}"
docker images
docker ps

echo "this is your private registry on the master node: ${DOCKER_REGISTRY}"
echo "kubectl is working with: ${KUBECONFIG}"
kubectl get nodes -o wide

# kubernetes stop
kube.sh stop

# kubernetes start
kube.sh start

# kubernetes status
kube.sh status
```