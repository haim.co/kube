# -*- mode: ruby -*-
# vi: set ft=ruby :

# https://github.com/aidanns/vagrant-reload/blob/master/README.md
# vagrant plugin install vagrant-reload
# vagrant plugin install vagrant-host-shell
# vagrant plugin install vagrant-parallels
# vagrant plugin install vagrant-scp
# vagrant plugin install vagrant-disksize
# vagrant plugin install vagrant-vbguest

# https://linuxconfig.org/how-to-install-kubernetes-on-ubuntu-20-04-focal-fossa-linux

# minikube kubectl no tls verification
# https://github.com/robertluwang/docker-hands-on-guide/blob/master/minikube-no-tls-verify.md

# remote docker daemon
# https://dockerlabs.collabnix.com/beginners/components/daemon/access-daemon-externally.html

unless Vagrant.has_plugin?("vagrant-reload")
    raise  Vagrant::Errors::VagrantError.new, "vagrant-reload plugin is missing. Please install it using 'vagrant plugin install vagrant-reload' and rerun 'vagrant up'"
end
unless Vagrant.has_plugin?("vagrant-host-shell")
    raise  Vagrant::Errors::VagrantError.new, "vagrant-host-shell plugin is missing. Please install it using 'vagrant plugin install vagrant-host-shell' and rerun 'vagrant up'"
end
unless Vagrant.has_plugin?("vagrant-scp")
    raise  Vagrant::Errors::VagrantError.new, "vagrant-scp plugin is missing. Please install it using 'vagrant plugin install vagrant-scp' and rerun 'vagrant up'"
end
unless Vagrant.has_plugin?("vagrant-disksize")
    raise  Vagrant::Errors::VagrantError.new, "vagrant-disksize plugin is missing. Please install it using 'vagrant plugin install vagrant-disksize' and rerun 'vagrant up'"
end
unless Vagrant.has_plugin?("vagrant-vbguest")
    raise  Vagrant::Errors::VagrantError.new, "vagrant-vbguest plugin is missing. Please install it using 'vagrant plugin install vagrant-vbguest' and rerun 'vagrant up'"
end


require 'yaml'
$vagrant_dir = File.dirname(__FILE__)
$cloud_playground_dir = File.realpath($vagrant_dir + "/../..")

$config_yml = $vagrant_dir + "/config.yml"
$config     = YAML.load_file($config_yml)
# STDERR.puts "config: #{$config}"

$opt_k8s     = $config["k8s"]
$vm_name     = $config["vm_name"]
$vm_cpu      = $config["vm_cpu"]
$vm_mem      = $config["vm_mem"]

$kernel_update = false
if $config["kernel_update"]
    $kernel_update = $config["kernel_update"]
end

$net_ip_fmt  = $config["network"]["ip_fmt"]
$net_ip_base = $config["network"]["ip_base"]
$master_ip   = $net_ip_fmt % [$net_ip_base]
$node_num    = $config["node_num"]

# if $config["mounts"]
#     $config["mounts"].each do |i|
#         STDERR.puts "#{i['host']}: #{i['guest']}"
#     end
# end

# STDERR.puts "$opt_k8s: #{$opt_k8s}"
# STDERR.puts "$vm_name: #{$vm_name}"
# STDERR.puts "$master_ip: #{$master_ip}"
#abort("She cannot take any more of this, Captain!")

$common_kernel_update = <<SCRIPT
echo "id:$(id)"
set -x
wget https://raw.githubusercontent.com/pimlie/ubuntu-mainline-kernel.sh/master/ubuntu-mainline-kernel.sh
chmod +x ubuntu-mainline-kernel.sh
./ubuntu-mainline-kernel.sh -i --yes
apt --fix-broken install -y
SCRIPT

$common_k8s_setup = <<-SHELL
# Common setup for all servers (Control Plane and Nodes)
set -euxo pipefail

# Variable Declaration
KUBERNETES_VERSION="1.23.6-00"

# disable swap
sudo swapoff -a

# keeps the swaf off during reboot
(crontab -l 2>/dev/null; echo "@reboot /sbin/swapoff -a") | crontab - || true
sudo apt-get update -y
# Install CRI-O Runtime

OS="xUbuntu_20.04"
VERSION="1.23"

# Create the .conf file to load the modules at bootup
cat <<EOF | sudo tee /etc/modules-load.d/crio.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# Set up required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system

cat <<EOF | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /
EOF
cat <<EOF | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list
deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /
EOF

curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/libcontainers.gpg add -
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/libcontainers.gpg add -

sudo apt-get update
sudo apt-get install cri-o cri-o-runc -y

sudo systemctl daemon-reload
sudo systemctl enable crio --now

echo "CRI runtime installed susccessfully"

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update -y
sudo apt-get install -y kubelet="$KUBERNETES_VERSION" kubectl="$KUBERNETES_VERSION" kubeadm="$KUBERNETES_VERSION"
sudo apt-get update -y
sudo apt-get install -y jq

local_ip="$(ip --json a s | jq -r '.[] | if .ifname == "eth1" then .addr_info[] | if .family == "inet" then .local else empty end else empty end')"
cat > /etc/default/kubelet << EOF
KUBELET_EXTRA_ARGS=--node-ip=$local_ip
EOF

sudo apt-get install -y meson python3-pyelftools

SHELL

$master_k8s_setup = <<-SHELL
# Setup for Control Plane (Master) servers
set -euxo pipefail

MASTER_IP="%s"
NODENAME=$(hostname -s)
POD_CIDR="192.168.0.0/16"

conf_path=/vagrant/conf
mkdir -p ${conf_path}

sudo rm -f ${conf_path}/config.sh ${conf_path}/config ${conf_path}/join.sh

cat > ${conf_path}/config.sh <<EOF
this_dir=\\$(cd \\$(dirname "\\${BASH_SOURCE[0]}");pwd)
bin_dir=\\$(cd \\${this_dir}/../bin;pwd)
this_file=\\$(basename \\${BASH_SOURCE[0]})
this_fullpath=\\${this_dir}/\\${this_file}

if [ \\$(echo \\${PATH} | grep \\${bin_dir} | wc -l) -eq 0 ];then
  export PATH=\\${bin_dir}:\\${PATH}
fi

export DOCKER_HOST=tcp://${NODENAME}:2375
export DOCKER_REGISTRY=${NODENAME}:5000
export KUBECONFIG=\\${this_dir}/config
EOF


sudo kubeadm config images pull

echo "Preflight Check Passed: Downloaded All Required Images"
sudo kubeadm init --apiserver-advertise-address=$MASTER_IP --apiserver-cert-extra-sans=$MASTER_IP --pod-network-cidr=$POD_CIDR --node-name "$NODENAME" --ignore-preflight-errors Swap

mkdir -p ${HOME}/.kube
sudo rm -f ${HOME}/.kube/config
sudo cp -f /etc/kubernetes/admin.conf ${HOME}/.kube/config
sudo chown "$(id -u)":"$(id -g)" ${HOME}/.kube/config

cp -i /etc/kubernetes/admin.conf ${conf_path}/config
touch ${conf_path}/join.sh
chmod +x ${conf_path}/join.sh

kubeadm token create --print-join-command > ${conf_path}/join.sh

# Install Calico Network Plugin
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.0/manifests/tigera-operator.yaml
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.0/manifests/custom-resources.yaml

sudo -i -u vagrant bash << EOF
whoami
mkdir -p /home/vagrant/.kube
sudo cp -i ${conf_path}/config /home/vagrant/.kube/
sudo chown 1000:1000 /home/vagrant/.kube/config
EOF

kubectl taint nodes "${NODENAME}" node-role.kubernetes.io/master:NoSchedule- || true
kubectl taint nodes "${NODENAME}" node-role.kubernetes.io/control-plane:NoSchedule- || true
#kubectl taint nodes --all node-role.kubernetes.io/control-plane- node-role.kubernetes.io/master- || true

SHELL

$node_k8s_setup = <<-SHELL
# Setup for Node servers
set -euxo pipefail

/bin/bash /vagrant/conf/join.sh -v

sudo -i -u vagrant bash << EOF
whoami
mkdir -p /home/vagrant/.kube
sudo cp -i /vagrant/conf/config /home/vagrant/.kube/
sudo chown 1000:1000 /home/vagrant/.kube/config
kubectl label node $(hostname -s) node-role.kubernetes.io/worker=worker
EOF
SHELL

$common_etc_host = <<-SHELL
vm_name=%s
ip_fmt=%s
ip_base=%d
node_num=%d

add_host()
{
    local ip=$1
    local name=$2
    if [ $(sudo cat /etc/hosts | grep "${name}" | wc -l) -ne 0 ];then
        cat /etc/hosts | sed 's/.* '"${name}"'/'"${ip}"' '"${name}"'/g' | sudo tee /etc/hosts
        return 0
    fi
    sudo bash -c "echo ${ip} ${name} >> /etc/hosts"
    return 0
}

add_host $(printf "${ip_fmt}" ${ip_base}) ${vm_name}-master
i=1
while [ ${i} -le ${node_num} ];do
  add_host $(printf "${ip_fmt}" $(( ${ip_base} + $i )) ) ${vm_name}-node${i}
  i=$(( ${i} + 1 ))
done
SHELL

$master_extra_tools = <<-SHELL
set -x

vm_name="%s"

apt-get update -y
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    git tmux vim netcat wget curl zip unzip net-tools iputils-ping \
    less jq atop docker.io apt-transport-https \
    clang gcc make gdb gdbserver valgrind libssl-dev dwarves \
    libelf-dev zlib1g binutils-multiarch-dev libcap-dev clang-12 \
    qemu-user qemu-user-static

echo "export EDITOR=vim" >> /home/vagrant/.profile

cat > /etc/docker/daemon.json <<EOF
{
    "hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"],
    "exec-opts": ["native.cgroupdriver=systemd"],
    "insecure-registries" : ["${vm_name}:5000"]
}
EOF

mkdir -p /etc/systemd/system/docker.service.d
cat > /etc/systemd/system/docker.service.d/override.conf <<EOF
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd
EOF

usermod -aG docker vagrant
systemctl daemon-reload
systemctl enable docker
systemctl restart docker

docker run -d -p 5000:5000 --restart=always --name registry registry:2

SHELL

$common_containerd_config = <<SCRIPT
echo "id:$(id)"
set -x
registry_name='%s'
echo "INFO registry_name=${registry_name}"
mkdir -p /etc/containerd
containerd config default | sed 's/\\(.*\\.mirrors\\]\\)/\\1\\n        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]\\n          endpoint = ["https:\\/\\/registry-1.docker.io"]\\n        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."'"${registry_name}"':5000"]\\n          endpoint = ["http:\\/\\/'"${registry_name}"':5000"]\\n/g' > /etc/containerd/config.toml
systemctl restart containerd

cp /etc/systemd/system/multi-user.target.wants/crio.service /tmp/crio.service
cat /tmp/crio.service | sed 's/\\(ExecStart=\\/usr\\/bin\\/crio\\)/\\1 --insecure-registry='"${registry_name}"':5000/g' > /etc/systemd/system/multi-user.target.wants/crio.service
sudo systemctl daemon-reload
sudo systemctl restart crio
SCRIPT

$host_config = <<SCRIPT
echo "id:$(id)"
set -x
sudo mkdir -p /etc/vbox/
echo "* 0.0.0.0/0 ::/0" | sudo tee /etc/vbox/networks.conf
SCRIPT


$common_resize_disk_space = <<SCRIPT
echo "id:$(id)"
set -x
if [ $(id -u) -ne 0 ];then
    echo "ERROR please run as root"
    exit 1
fi
# https://stackoverflow.com/a/66117279/3843496
if [ $(vgdisplay | grep Free | grep GiB$ | wc -l) -ne 1 ];then
    echo "INFO no need to resize"
    exit 0
fi

size_gb=$(vgdisplay | grep Free | grep GiB$ | awk '{print $7}')
echo "INFO extra disk size available ${size_gb}GB"
lvm_mapper=$(df | grep ^/dev/mapper/ | awk '{print $1}')
if [ "${lvm_mapper}"x = ""x ];then
    echo "ERROR failed to identify lvm mapper"
    exit 1
fi

# extend lvm group
lvextend  -L+${size_gb}GB ${lvm_mapper}
if [ $? -ne 0 ];then
    echo "ERROR failed to extend ${lvm_mapper} by ${size_gb}GB"
    exit 1
fi
resize2fs ${lvm_mapper}
if [ $? -ne 0 ];then
    echo "ERROR failed to resize ${lvm_mapper}"
    exit 1
fi

df -h | grep ${lvm_mapper}
SCRIPT


$vm_image_name = "bento/ubuntu-22.04"

Vagrant.configure("2") do |config|
    master_name = $vm_name + "-master"
    config.vm.provision "update disk size",       type: "shell",      run: 'always', inline: $common_resize_disk_space
    config.vm.provision "update vbox net",        type: "host_shell", run: 'always', inline: $host_config
    config.vm.provision "update host /etc/hosts", type: "host_shell", run: 'always', inline: $common_etc_host % [$vm_name, $net_ip_fmt, $net_ip_base, $node_num]
    config.vm.provision "update node /etc/hosts", type: "shell",      run: 'always', inline: $common_etc_host % [$vm_name, $net_ip_fmt, $net_ip_base, $node_num]

    config.vm.define master_name do |master|
        master.vm.box = $vm_image_name
        master.vm.box_check_update = true
        master.vm.hostname = master_name
        master_ip = $net_ip_fmt % [ $net_ip_base ]
        master.vm.network "private_network", ip: master_ip
        if $config["network"]["forwards"]
            $config["network"]["forwards"].each do |i|
                master.vm.network "forwarded_port", guest: i['guest'], host: i['host'], protocol: i['protocol']
            end
        end

        master.disksize.size = "64GB"
        if $config["mounts"]
            $config["mounts"].each do |i|
                master.vm.synced_folder i['host'], i['guest']
            end
        end

        master.vm.provider "virtualbox" do |vb,override|
            vb.name = master_name
            vb.memory = $vm_mem
            vb.cpus = $vm_cpu
        end

        if $kernel_update
            master.vm.provision "common kernel update",    type: "shell", inline: $common_kernel_update
            master.vm.provision :reload
        end
        master.vm.provision "common k8s setup",        type: "shell", inline: $common_k8s_setup
        master.vm.provision "master k8s setup",        type: "shell", inline: $master_k8s_setup % [ master_ip ]
        master.vm.provision "extra tools",             type: "shell", inline: $master_extra_tools % [ master_name ]
        master.vm.provision "make registry available", type: "shell", inline: $common_containerd_config % [ master_name ]
    end

    (1..$node_num).each do |i|
        node_name = $vm_name + "-node%d" % i
        config.vm.define node_name do |node|
            node.vm.box = $vm_image_name
            node.vm.box_check_update = true
            node.vm.hostname = node_name
            node.vm.network "private_network", ip: $net_ip_fmt % [ $net_ip_base + i ]
            node.vm.provider "virtualbox" do |vb|
                vb.name = node_name
                vb.memory = 2048
                vb.cpus = 1
            end
            if $kernel_update
                node.vm.provision "common kernel update",    type: "shell", inline: $common_kernel_update
                node.vm.provision :reload
            end
            node.vm.provision "common k8s setup",        type: "shell", inline: $common_k8s_setup
            node.vm.provision "node   k8s setup",        type: "shell", inline: $node_k8s_setup
            node.vm.provision "make registry available", type: "shell", inline: $common_containerd_config % [ master_name ]
        end
    end
end
